package kodune;

//Kirjutada programm, mis viskab 5 t�ringut ning arvutab silmade summa
//NB! Lahendus peab olema tehtud ts�kliga.
//Vihje: ts�klis summa arvutamiseks tuleb summat sisaldav muutuja enne ts�kli
//k�ivitamist v��rtustada nulliga ja ts�kli kehas liita sellele �ks haaval v��rtusi otsa (sum = sum + ...)

public class ViisT2ringut {

	public static void main(String[] args) {

		int summa = 0;

		for (int i = 0; i < 5; i++)
					{
			summa = summa + KaheT2ringuM2ng.t2ring();

		}
		System.out.println("Viskasid kokku: " + summa);

	}

}
