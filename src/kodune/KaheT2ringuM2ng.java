package kodune;

//Kirjutada t�ringum�ng:
//Programm viskab kaks t�ringut m�ngijale ja kaks t�ringut endale (arvutile), 
//arvutab m�lema m�ngija silmade summad ja teatab, kellel oli rohkem.

import java.util.Random;

public class KaheT2ringuM2ng {

	public static void main(String[] args) {
		// kaks korda tuleb t�ringu meetod v�lja kutsuda ja tulemusi v�rrelda

		int m2ngija = t2ring() + t2ring();
		System.out.println("Viskasid kokku: " + m2ngija);
		int arvuti = t2ring() + t2ring();
		System.out.println("Arvuti viskas: " + arvuti);

		if (m2ngija > arvuti) {
			System.out.println("Sina v�itsid!");
		} else if (arvuti > m2ngija) {
			System.out.println("Arvuti v�itis!");
		} else {
			System.out.println("M�ng j�i viiki!");
		}

	}// main

	public static int t2ring() {

		int min = 1;
		int max = 6;

		Random foo = new Random();
		int randomNumber = foo.nextInt((max + 1) - min) + min;

		return randomNumber;
	}

}// klass
