package kodune;

//Kirjutada liisu t�mbamise programm.
//K�sida kasutajalt inimeste arv. 
//V�ljastada random number vahemikus 1 kuni arv (kaasaarvatud)
//NB! Kontrollida, et t��tab �igesti: st. �eldes mitu korda j�rjest arvuks 3, 
//peab v�imalike vastuste hulgas olema nii �htesid, kahtesid kui kolmi.

import java.util.Random;

public class Liisk {

	public static void main(String[] args) {

		int osalejad;
		do {
			System.out.print("Palun sisesta osalejate arv: ");
			osalejad = TextIO.getInt();
		} while (osalejad <= 0);

		System.out.print("Liisk langes osaleja number " + liisk(1, osalejad) + ".");

	}

	public static int liisk(int min, int osalejad) {

		Random foo = new Random();
		int randomNumber = foo.nextInt((osalejad + 1) - min) + min;

		return randomNumber;
	}

}
