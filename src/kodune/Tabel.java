package kodune;

public class Tabel {
	public static void main(String[] args) {

		for (int j = 0; j < 10; j++) {
			for (int i = 0; i < 10; i++) {
				if (i + j < 10) {
					System.out.print(i + j + " ");
				} else {
					System.out.print((i + j) - 10 + " ");
				}

				// System.out.print("(j=" + j + " i=" + i + ") ");
			} // For i

			System.out.println();
		} // for j
	}// mainmethod

}// klass
