package kodune;

public class MinMax {

	public static void main(String[] args) {

		System.out.print("Sisestatud arv on " + sisendArv(3, 7) + ".");

	}// main

	public static int sisendArv(int min, int max) {

		int sisestus;
		do {
			System.out.println("Palun sisesta arv vahemikus " + min + " kuni " + max + ": ");
			sisestus = TextIO.getlnInt();
		} while (sisestus < min || sisestus > max);
		return sisestus;
	}// sisendArv

}// klass
