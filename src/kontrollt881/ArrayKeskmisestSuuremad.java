package kontrollt881;

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d p�hjal niisuguste elementide arvu, 
//mis on rangelt suuremad k�igi elementide aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv). 

//Write a method in Java to find the number of elements strictly greater than arithmetic mean of all elements 
//of a given array of real numbers d (arithmetic mean = sum_of_elements / number_of_elements).

public class ArrayKeskmisestSuuremad {

	   public static void main (String[] args) {
	      System.out.println (keskmisestParemaid (new double[]{0.}));
	      // YOUR TESTS HERE
	   } //main

	   public static int keskmisestParemaid (double[] d) {
		   
		   if (d.length > 0){
		   int sum = 0;
		   for (int i = 0; i < d.length; i++){
			   sum += d[i];
		   }
		   int count = 0;
		   int keskmine = sum/d.length;
		  for (int j = 0; j < d.length; j++){
			  if(d[j] > keskmine){
				  count += 1;
			  }//if
		  }//for
		   
		  return count;  // YOUR PROGRAM HERE
		   }else{
			   throw new RuntimeException ("\n Massiivis ei ole elemente.");
		   }
	   }//keskmisestParemaid

	}//klass

