package kontrollt881;

//Koostage Java-meetod, mis leiab etteantud massiivi m rangelt positiivsete elementide arvu. 
//Write a method in Java to find the number of strictly positive elements of a given array m.

public class ArrayPositiivsedElemendid {

	   public static void main (String[] args) {
	      System.out.println (posElArv (new int[]{0}));
	      // YOUR TESTS HERE
	   }

	   public static int posElArv (int[] m) {
		   
		   int count = 0;
		   for (int i = 0; i < m.length; i++){
			   if (m[i] > 0){
				   count += 1;
			   }
		   }
	      return count;  // YOUR PROGRAM HERE
	   }

	}