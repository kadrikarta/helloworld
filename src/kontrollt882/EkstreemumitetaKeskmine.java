package kontrollt882;

//Sportlase esinemist hindab n>2 kohtunikku. 
//Hinnete hulgast eemaldatakse kõige madalam ja kõige kõrgem ning 
//leitakse ülejäänud n-2 hinde aritmeetiline keskmine. 
//Kirjutada Java-meetod hinde arvutamiseks.

public class EkstreemumitetaKeskmine {

	public static void main(String[] args) {
		System.out.println(result(new double[] { 0., 1., 2., 3., 4. }));
		// YOUR TESTS HERE
	} // main

	public static double result(double[] marks) {

		double min = marks[0];
		for (int i = 0; i < marks.length; i++) {
			if (marks[i] < min) {
				min = marks[i];
			} // if
		} // for
		//System.out.println("Miinimum: " + min);

		double max = marks[0];
		for (int j = 0; j < marks.length; j++) {
			if (marks[j] > max) {
				max = marks[j];
			} // if
		} // for
		//System.out.println("Maksimum: " + max);

		double eemaldamiseks = min + max;

		double sum = 0;
		for (int k = 0; k < marks.length; k++) {
			sum = sum + marks[k];
		} // for

		double kokku = sum - eemaldamiseks;
		//System.out.println("Summa ilma min ja max: " + kokku);
		
		double keskmine = kokku / (marks.length -2);

		return keskmine; // TODO!!! YOUR PROGRAM HERE
	}

}
