package kontrollt882;

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal niisuguste elementide arvu, 

//mis on rangelt väiksemad kõigi elementide aritmeetilisest keskmisest 
//(aritmeetiline keskmine = summa / elementide_arv). 

public class KeskmistestV2iksemad {

	public static void main(String[] args) {
		System.out.println (allaKeskmise (new double[]{0.}));
		//System.out.println(allaKeskmise(new double[] { 0, 5, 6, 9, 2, 4, 5, 1, 9, 2 }));
	} // main

	public static int allaKeskmise(double[] d) {

		if (d.length > 0) {
			double sum = 0;
			for (int i = 0; i < d.length; i++) {
				sum += d[i];
			}
			double count = 0;
			double keskmine = sum / d.length;
			//System.out.println(keskmine);
			for (int j = 0; j < d.length; j++) {
				if (d[j] < keskmine) {
					count = count + 1;
				} // if
			} // for

			return (int)count; // YOUR PROGRAM HERE
		} else {
			throw new RuntimeException("\n Massiivis ei ole elemente!");
		}
	}// allaKeskmise

}
