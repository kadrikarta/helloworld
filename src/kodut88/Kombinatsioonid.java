package kodut88;

/**
 * Created by Kadri on 04.12.2015.
 */
public enum Kombinatsioonid {
    YHED {
        @Override
        public String toString() {
            return "1 - �hed";
        }
    },
    KAHED {
        @Override
        public String toString() {
            return "2 - Kahed";
        }
    },
    KOLMED {
        @Override
        public String toString() {
            return "3 - Kolmed";
        }
    },
    NELJAD {
        @Override
        public String toString() {
            return "4 - Neljad";
        }
    },
    VIIED {
        @Override
        public String toString() {
            return "5 - Viied";
        }
    },
    KUUED {
        @Override
        public String toString() {
            return "6 - Kuued";
        }
    },
    PAAR {
        @Override
        public String toString() {
            return "7 - Paar";
        }
    },
    KAKSPAARI {
        @Override
        public String toString() {
            return "8 - 2 paari";
        }
    },
    KOLMIK {
        @Override
        public String toString() {
            return "9 - Kolmik";
        }
    },
    NELIK {
        @Override
        public String toString() {
            return "10 - Nelik";
        }
    },
    VRIDA {
        @Override
        public String toString() {
            return "11 - V�ike rida";
        }
    },
    SRIDA {
        @Override
        public String toString() {
            return "12 - Suur rida";
        }
    },
    MAJA {
        @Override
        public String toString() {
            return "13 - Maja";
        }
    },
    SUMMA {
        @Override
        public String toString() {
            return "14 - Summa";
        }
    },
    YATZY {
        @Override
        public String toString() {
            return "15 - Yatzy";
        }
    };

    public static Kombinatsioonid fromInt(int i) {
        switch (i) {
            case 1:
                return YHED;
            case 2:
                return KAHED;
            case 3:
                return KOLMED;
            case 4:
                return NELJAD;
            case 5:
                return VIIED;
            case 6:
                return KUUED;
            case 7:
                return PAAR;
            case 8:
                return KAKSPAARI;
            case 9:
                return KOLMIK;
            case 10:
                return NELIK;
            case 11:
                return VRIDA;
            case 12:
                return SRIDA;
            case 13:
                return MAJA;
            case 14:
                return SUMMA;
            case 15:
                return YATZY;

        }

        return null;
    }
}
