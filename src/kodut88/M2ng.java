package kodut88;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Kadri on 13.11.2015.
 */
public class M2ng {

    /**
     * MAX_VOORUD M22rab m2ngu voorude arvu. Yatzy kohaselt on neid 15.
     */
    public final static int MAX_VOORUD = 15;

    /**
     * M2ng.
     */
    public M2ng() {

        run();
    }

    /**
     * Yatzy peamine kood.
     */
    private void run() {
        int summa1 = 0; //m�ngija 1 kogu punktisumma
        int summa2 = 0; //m�ngija 2 kogu punktisumma
        int kord;

        System.out.println("Teretulemast! M�ngime Yatzyt!");
        System.out.println("M�ngija 1.");
        String m2ngija1 = nimed();
        System.out.println("M�ngija 2.");
        String m2ngija2 = nimed();

        //M�ngijate vabade kombinatsioonide nimekirjad.
        LinkedHashMap<Kombinatsioonid, Boolean> m2ngija1kombod = new LinkedHashMap<Kombinatsioonid, Boolean>();
        LinkedHashMap<Kombinatsioonid, Boolean> m2ngija2kombod = new LinkedHashMap<Kombinatsioonid, Boolean>();

        for (Kombinatsioonid c : Kombinatsioonid.values()) {
            m2ngija1kombod.put(c, false);
        }

        for (Kombinatsioonid c : Kombinatsioonid.values()) {
            m2ngija2kombod.put(c, false);
        }

        //m�ngu voorud
        for (int u = 0; u < MAX_VOORUD; u++) {
            kord = u + 1;
            System.out.println();
            System.out.println("Kord nr: " + kord);

            //m�ngija 1 v�i 2
            for (int m = 0; m < 2; m++) {
                if (m == 0) System.out.println("M�ngija " + m2ngija1 + " kord.");
                if (m == 1) System.out.println("M�ngija " + m2ngija2 + " kord.");

                Vise vise = new Vise();

                vise.viska();                   //esmane vise
                vise.prindiTulemus(1);          //Ytlen kasutajale, mis ta viskas
                vise.yatzyKontroll();           //Kontrollin, kas kasutaja viaskas Yatzy


                System.out.println("Kas soovid m�ne t�ringu uuesti veeretada?");

                while (true) {
                    System.out.println("Jah (Y) v�i ei (N)?");
                    String vastus = TextIO.getlnString();

                    if (vastus.equalsIgnoreCase("y")) {

                        veeretaUuesti(vise);
                        break;

                    } else if (vastus.equalsIgnoreCase("n")) {
                        break;
                    }
                }//Sisestada saab ainult Y v�i N

                System.out.println("Sinu l�plik tulemus on: ");
                vise.prindiTulemus(0);

                System.out.println("Vabad on veel: ");
                if (m == 0) trykiKasutamata(m2ngija1kombod);
                if (m == 1) trykiKasutamata(m2ngija2kombod);

                System.out.println();
                System.out.println("Millise kombinatsioonina soovid tulemuse kirja panna? ");

                //nr - vastava Kombinatsiooni j�rjekorranr.
                int nr;

                while (true) {
                    System.out.println("Sisesta vastava kombinatsiooni nr 1 - 15");
                    nr = TextIO.getlnInt();
                    if (nr > 0 && nr < 16) {
                        break;
                    }
                }

                int tulemus = vise.v22rtusta(nr);

                System.out.println("Said selle eest " + tulemus + " punkti.");

                //Eemaldan skooritud tulemuse �ige m�ngija vabade variantide hulgast.
                if (m == 0){
                    summa1 += tulemus;
                    System.out.println("Kokku punkte: " + summa1);
                    eemaldaKombinatsioon(nr, m2ngija1kombod);
                }
                if (m == 1) {
                    summa2 += tulemus;
                    System.out.println("Kokku punkte: " + summa2);
                    eemaldaKombinatsioon(nr, m2ngija2kombod);
                }

                //T�hjad read, et m�ngu kulgu oleks lihtsam j�lgida.
                for (int v = 0; v < 5; v++){
                    System.out.println();
                }

            }//m�ngija 1 ja 2 korrad vooru sees

            System.out.println("Selle vooru l�pus punktiseis...");
            System.out.println(m2ngija1 + ": " + summa1);
            System.out.println(m2ngija2 + ": " + summa2);

            //T�hjad read, et m�ngu kulgu oleks lihtsam j�lgida.
            for (int w = 0; w < 5; w++){
                System.out.println();
            }


        }//Suur FOR, reguleerib m�ngu voorusid.

        System.out.println("L�pptulemus ");
        System.out.println(m2ngija1 + ": " + summa1);
        System.out.println(m2ngija2 + ": " + summa2);
    }

    /**
     * Meetod eemaldab m�ngija poolt skoorimiseks valitud kombinatsiooni vabade variantide nimekirjast.
     * @param nr M�ngu kombinatsioonile vastav j�rjekorranumber.
     * @param hasBeenUsed Kasutatud kombinatsioon.
     */
    private void eemaldaKombinatsioon(int nr, HashMap<Kombinatsioonid, Boolean> hasBeenUsed) {
        hasBeenUsed.put(Kombinatsioonid.fromInt(nr), true);

    }

    /**
     * Meetod tr�kib v�lja vabad varandid, mida saab veel skoorida.
     * @param skooritud M�ngija poolt skooritud kombinatsioon.
     */
    private void trykiKasutamata(HashMap<Kombinatsioonid, Boolean> skooritud) {
        for (Kombinatsioonid c : skooritud.keySet()) {
            if(!skooritud.get(c)) {
                System.out.println(c.toString());
            }
        }
    }

    /**
     * Meetod laseb m�ngijal emsasest viskest soovitud t�ringud uuesti veeretada.
     * @param originaalvise M�ngija esmane t�ringuvise.
     */
    public static void veeretaUuesti(Vise originaalvise){


        int t2ring = 0;
        System.out.println("Iga t�ringu kohta: Y- veereta uuesti, N - ei soovi veeretada");

        boolean t1 = false;
        boolean t2 = false;
        boolean t3 = false;
        boolean t4 = false;
        boolean t5 = false;

        for (int i = 0; i < 5; i++) {
            t2ring = t2ring + 1;

            System.out.println("T�ring " + t2ring + "? ");
            String vastus1 = TextIO.getlnString();
            if (vastus1.equalsIgnoreCase("y")) {
                if (t2ring == 1) {
                    t1 = true;
                } else if (t2ring == 2) {
                    t2 = true;
                } else if (t2ring == 3) {
                    t3 = true;
                } else if (t2ring == 4) {
                    t4 = true;
                } else if (t2ring == 5) {
                    t5 = true;
                }
            }
        }


        originaalvise.viska(t1, t2, t3, t4, t5);

    }//veeretaUuesti

    /**
     * Meetod laseb m�ngijatel oma nimed sisestada, et neid edaspidi m�ngus kasutada.
     * @return M�ngija sisestatud nimi.
     */
    public static String nimed(){

        String nimi;

        System.out.println("Palun sisesta oma nimi: ");
        nimi = TextIO.getlnString();

        return nimi;
    }

    /**
     * See meetod loob uue M2ng objekti.
     */
    public static void main(String[] args){
        new M2ng();
    }

}//klass

