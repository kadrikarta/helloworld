package materjalid;
public class Kogumid {
}

//Collection
//	Set
//		SortedSet
//			TreeSet (korduvate elementideta järjestatud hulk)
//	List
//		ArrayList (vektor, dünaamiline indekseeritav kogum, korduvad elemendid lubatud)
//
//Map
//	HashMap (paisktabel, paaride "võti - väärtus" salvestamiseks; kujutis, mis seab võtmele vastavusse väärtuse)
//
//Iterator (liides andmekogumi elementide süstemaatiliseks läbikäimiseks)
//
//Comparable (liides kahe elemendi omavahelise järjestuse määramiseks)
//Collections  (klassimeetodite kogum andmekogumitega manipuleerimiseks)