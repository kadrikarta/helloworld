package materjalid;

public class Tyybid {

}
//
//Algt��bid (ingl.k. primitive types)
//
//byte - 8-bitine t�isarv vahemikus -128 kuni 127. N�it. (byte)31, (byte)0x1f,(byte)037
//
//short - 16-bitine t�isarv vahemikus -32768 kuni 32767. N�it. (short)255, (short)0xff,(short)0377
//
//int - 32-bitine t�isarv vahemikus -2147483648 kuni 2147483647. N�it. -25, 0x84ed1,0265475,1234567890
//
//long - 64-bitine t�isarv vahemikus -9223372036854775808 kuni 9223372036854775807. N�it. 12L,-0xfcd45dL,03451207564302L
//
//float - 32-bitine ujupunktarv ligikaudses vahemikus -3,4x1038 kuni 3,4x1038 seitsme t�vekohaga. N�it. -5.4F, 32e+17F, 0.5e-6F
//
//double - 64-bitine ujupunktarv ligikaudses vahemikus -1,7x10308 kuni 1,7x10308 15 t�vekohaga. N�it. 8.3, -5e-100
//
//char - 16-bitine Unicode s�mbol. N�it. 'a', '\n', \u00ff
//
//boolean - t�ev��rtust��p v�imalike v��rtustega true ja false.
//T�hit��p
//
//void - tagastust��p meetoditele, mis v��rtust ei tagasta.


//
//S�ned
//Keeles Java on s�ned (ingl.k. string) "t�elised" objektid, mis kuuluvad klassi java.lang.String. 
//N�it. "See on s�ne", "Kaks\nrida". Selle klassi olulisteks isendimeetoditeks on 
//compareTo(), equals(), length(), concat(), toString(), indexOf(), substring(), replace(), trim(), jpt. 
//ning klassimeetodiks valueOf()(vt. API kirjeldus).