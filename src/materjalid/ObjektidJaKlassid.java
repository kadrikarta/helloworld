package materjalid;
public class ObjektidJaKlassid {
}

// Iga objekt kuulub mingisse klassi; ta on selle klassi isend (ingl.k. instance).

// Sama klassi isenditel on samad isendimuutujad(ingl.k.instance variables) ja isendimeetodid (ingl.k. instance methods), 
// isendimuutujate v��rtused (olek) on �ldjuhul erinevad (kui langevadki kokku, siis m��rab isendi tema identiteet).

// Klassimuutujad on �hised k�igile isenditele (tavaliselt konstandid, n�iteks kassi jalgade arv - 4) ja neid saab kasutada 
// nii klassimeetodites kui ka isendimeetodites. 

// Konstruktor on erikujuline klassimeetod uute isendite loomiseks (new, malloc, ...), n�iteks nn. "vabrik" (Javas Klass.getInstance()). 
// Destruktor (free) isendite h�vitamiseks on vahel (n�it. Javas) asendatud m�lukoristusvahenditega (ingl.k. garbage collection).

// P�rimine (ingl.k. inheritance)
// Objektide omadusi saab kirjeldada erinevatel tasemetel (n�it. Miisu omadused, kasside omadused, imetajate omadused, ...). 
// �ldistamine (ingl.k. generalization) lubab objektide sarnaseid omadusi kirjeldada k�rgemal tasemel 
// (n�it. kehatemperatuur ei ole iseloomulik atribuut ainult kassidele, vaid k�igile imetajatele). 
// Spetsialiseerimine (ingl.k. specialization) on vastupidine protsess - me toome v�lja teatud eriomadused, 
// mida ei saa kirjeldada k�rgemal tasemel.

// Atribuutide hulga seisukohalt vaadates alamklass laiendab (ingl.k. extends) antud klassi - lisanduvad mingid spetsiifilised atribuudid, 
// t�psustuvad kasutatavad meetodid (NB!  Mitte segi ajada - kass ei ole laiem m�iste kui imetaja, tal on lihtsalt rohkem atribuute).