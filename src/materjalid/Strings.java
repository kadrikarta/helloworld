package materjalid;
public class Strings {
}

//N�ide. Loetleda s�mbolite 'a' arv etteantud s�nes.
//
//public static int adeArv (String s) {
//   int result = 0;
//   if (s != null) {
//      for (int i=0; i<s.length(); i++) {
//         if (s.charAt (i) == 'a') result++;
//      }
//   }
//   return result;
//} // adeArv



//N�ide. Moodustada s�ne j�rjestikustest t�htedest  'a' kuni 'z'.
//
//StringBuffer puhver = new StringBuffer();
//
//for (char c = 'a'; c <= 'z'; c++)
//   puhver.append (c);
//
//String tulemus = puhver.toString();



//String-klassi klassimeetod valueOf aitab teisendada algt��pi v��rtusi s�neks:
//
//double d = TextIO.getlnDouble();
//String arvTekstina = String.valueOf (d);
//
//Vastupidised teisendused leiduvad algt��bile vastavate m�hisklasside juures:
//
//double d1 = Double.parseDouble (arvTekstina);




//N�ide. S�nade sagedustabeli moodustamine (anal��sime p�hjalikumalt hiljem).
//
//import java.util.*;
//
//public class Sagedused {
//
//   public static void main (String[] arg) {
//      
//      Hashtable stabel = leiaSagedused (arg);
//      Object [] om = stabel.keySet().toArray();
//      Arrays.sort (om);
//      for (int i=0; i<om.length; i++) {
//         String sona = (String)om[i];
//         int sagedus = ((Integer)stabel.get (sona)).intValue();
//         System.out.println (sona + " " + String.valueOf (sagedus));
//      }
//   } // main
//
//   public static Hashtable leiaSagedused (String[] t) {
//      Hashtable result = new Hashtable();
//      for (int i=0; i<t.length; i++) {
//         StringTokenizer st = new StringTokenizer (t[i]);
//         while (st.hasMoreTokens()) {
//            String word = st.nextToken();
//            if (result.containsKey (word)) {
//               int k = ((Integer)result.get (word)).intValue();
//               result.put (word, new Integer (k+1));
//            } else {
//               result.put (word, new Integer (1));
//            }
//         }
//      }
//      return result;
//   } // leiaSagedused
//
//} // Sagedused
