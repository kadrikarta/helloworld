package materjalid;

public class Keeled {

}
//Ajalooliselt esimesed kõrgtaseme keeled toetavad imperatiivset lähenemist.
//FORTRAN, kirjeldus 1954, realisatsioon 1956. J. Backus. FORmula TRANslator
//ALGOL, 1958, P. Naur. ALGOrithmic Language - Euroopa projekt
//COBOL, 1959, COmmon Business Oriented Language - USA
//BASIC, 1965, Beginners All-purpose Symbolic Instruction Code - USA
//Pascal, 1971, N. Wirth - Euroopa
//C, 1974, D. Ritchie
//Ada, 1979 - USA

//Funktsionaalsetest keeltest esimene on Lisp, 1962, J. McCarthy, LISt Processing - MIT

//Loogilistest keeltest esimene on Prolog, 1971, PROgramming in LOGic - Marseille Univ.

//OOP alused Simula, 1967
//Smalltalk - "puhas" OOP
//C++ , 1986, B. Stroustrup, OOP
//ML, Haskell, Scheme - funktsionaalsed
//Java, 1995, Sun - OOP
//
//
//Programmeerimiskeelt iseloomustavad:
//leksika - kuidas panna kirja elementaarseid "sõnu" antud keeles - nimed (identifikaatorid), 
//konstandid (arvud, stringid, tõeväärtused jne.),  võtmesõnad (reserveeritud nimed), eraldajad jne.
//süntaks - antud keele grammatikareeglid. Erinevalt loomulikest keeltest ei ole programmeerimiskeeltes 
//mitte midagi peale hakata süntaktiliselt vigaste tekstidega.
//semantika - keelekonstruktsioonide tähendus, s.t. kuidas interpreteeritakse süntaktiliselt korrektset programmi.
//stiil ja programmide koostamise metoodika. On kokkulepped, millega vabatahtlikult kitsendatakse süntaktiliselt 
//lubatud programmide hulka, et saavutada paremat loetavust inimese poolt (näit. "treppimine" programmi struktuuri väljatoomiseks, 
//nimekokkulepped jne.).
