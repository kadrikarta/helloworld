package materjalid;

public class Masiiv {
	//
	// int [] m; // massiivi kirjeldamine
	//
	// m = new int [10]; // mälu reserveerimine massiivile
	//
	// System.out.println (m.length); // massiivi pikkuse väljastamine
	//
	// m[0] = 3; // omistamine elemendile indeksiga 0
	// m[1] = -8;
	//
	//// massiivi väljastamine for-tsükli abil
	// for (int i=0; i<m.length; i++)
	// System.out.print (String.valueOf (m[i]) + " ");
	// System.out.println();
	//
	//
	//
	// Massiivi kirjeldamist, mälu eraldamist ja massiivi elementide
	// väärtustamist saab Javas teha korraga nn. massiivialgati abil.
	//
	// int [] a = {3, -8, 0, 0, -1, 0, 0};
	//
	//
	// Luuakse 7-elemendiline täisarvude massiiv loetletud väärtustest (
	// a[0]==3, a[1]==-8, ..., a[6]==0 ).
	//
	// Seda saab kirjutada ka massiivitüüpi avaldisena:
	// new int [] {3, -8, 0, 0, -1, 0, 0}

	public static void main(String[] p) {       //Kahemõõtmeline massiiv!!!
		int[][] m; // massiivi kirjeldamine
		m = new int[2][]; // m2lu reserveerimine esimesel tasemel
		System.out.println(m.length); // massiivi pikkus
		m[0] = new int[4]; // m2lu rerveerim. teisel tasemel
		m[0][0] = -8; // omistamine elemendile
		m[1] = new int[3];
		m[1][0] = 9;

		// massiivi v2ljastamine for-tsyklite abil
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++)
				System.out.print(String.valueOf(m[i][j]) + " ");
			System.out.println();
		} // for i
		System.out.println();

	} // main
}
