package Eksam;

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal niisuguste elementide arvu, 
//mis on rangelt suuremad kõigi elementide aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv). 

public class KeskmisestSuuremad {
	
	public static void main(String[] args) {
		System.out.println(keskmisestParemaid(new double[] { 0. }));

		// YOUR TESTS HERE
		// System.out.println (keskmisestParemaid (new double[]{0, 2, 7, -3, -7,
		// 10, -11}));
	}

	public static int keskmisestParemaid(double[] d) {

		double sum = 0;

		for (int i = 0; i < d.length; i++) {
			sum += d[i];
		}

		int count = 0;

		double keskmine = sum / d.length;

		for (int j = 0; j < d.length; j++) {
			if (d[j] > keskmine) {
				count += 1;
			}

		}
		// System.out.println(keskmine);

		return (int) count;
	}

}
