package Eksam;

//Koostage Java meetod etteantud täisarvumaatriksi m reasummade massiivi leidmiseks (
//massiivi i-s element on maatriksi i-nda rea summa). Reapikkused võivad olla erinevad.

public class ReaSummad {

	public static void main(String[] args) {
		int[] res = reaSummad(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } }); // {6,
																			// 15}
		// YOUR TESTS HERE
//		for (int i : res) {
//			System.out.println(i);
//		}

	}

	public static int[] reaSummad(int[][] m) {

		int summa = 0;
		int[] summad = new int[m.length];
		int count = 0;

		for (int i = 0; i < m.length; i++) {

			for (int j = 0; j < m[i].length; j++) {

				summa += m[i][j];
				summad[count] = summa;
			}

			count++;
			summa = 0;
		}

		return summad;
	}

}
