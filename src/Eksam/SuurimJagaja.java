package Eksam;

//On antud positiivne täisarv n. Kirjutada Java meetod, mis leiab n suurima algarvulise jagaja.
//Integer n is positive. Write a Java method to find the greatest primal divisor of n. 

public class SuurimJagaja {

	public static void main(String[] args) {
		System.out.println(greatestPrimeFactor(1234)); // 617
//		System.out.println(greatestPrimeFactor(2)); // 2
//		System.out.println(greatestPrimeFactor(4)); // 2
//		System.out.println(greatestPrimeFactor(123456789)); // 3803

	}

	public static int greatestPrimeFactor(int n) {

		int suurimJagaja = 0;
		int jagaja = 0;

		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				if (i == 1 || i == 2 || i == 3|| i == 5 || i == 7)
					suurimJagaja = i;
				else if (!(i % 2 == 0) && !(i % 3 == 0) && !(i % 5 == 0) && !(i % 7 == 0))
					suurimJagaja = i;
			}

		}

		return suurimJagaja; // TODO!!! Your code here!
	}

}
