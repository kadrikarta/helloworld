package Eksam;

//Koostage Java-meetod, mis leiab etteantud massiivi m negatiivsete elementide summa.
//Write a method in Java to find the sum of negative elements of a given array m. 

public class NegatiivseteSumma {
	
	public static void main (String[] args) {
	      System.out.println (negSumma (new int[]{0}));
	      
	      // YOUR TESTS HERE
	      //System.out.println (negSumma (new int[]{0, 2, 7, -3, -7, 10, -11}));
	      
	      
	   }

	   public static int negSumma (int[] m) {
		   
		   int sum = 0;
		  
		   for (int i = 0; i < m.length; i++){
			   if (m[i] < 0){
				   sum += m[i];
				   
				   
			   }
		   }
		   
	      return sum;  
	   }


}
