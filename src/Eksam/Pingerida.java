package Eksam;


//On antud hinnete maatriks (int[][] g), milles on iga üliõpilase jaoks üks rida, 
//mille elementideks on selle üliõpilase hinded (skaalal 0 kuni 5). 
//Koostada Java meetod üliõpilaste pingerea moodustamiseks, mis tagastaks reanumbrite massiivi 
//(kõrgeima keskhindega reast allapoole, võrdsete korral jääb ettepoole see rida, mille number on väiksem). 

import java.util.Arrays;
public class Pingerida {

	public static void main(String[] args) {
		int[][] grades = new int[][] { { 5, 3, 1 }, { 4, 3, 5 } };
		int[] res = sortByAvg(grades); // {1,0}
		for (int i = 0; i < res.length; i++) {
			System.out.print(res[i] + " ");
		}
	} // main

	public static int[] sortByAvg(int[][] g) {

		int[] pingerida = new int[g.length];

		int summa = 0;
		int[][] keskmised = new int[g.length][];
		int count = 0;
		int rida = 0;

		for (int i = 0; i < g.length; i++) {

			for (int j = 0; j < g[i].length; j++) {

				summa += g[i][j];
			}
			keskmised[count][rida] = summa / g[i].length;

			count++;
			summa = 0;
		}
		Arrays.sort(keskmised);

		for (int u = 0; u < keskmised.length; u++) {

			pingerida[u] = keskmised[u][0];
			
		}
		
	

		return pingerida; // TODO!!! Your code here
	} // sortByAvg

}
